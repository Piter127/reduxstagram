const webpack = require('webpack');
const path = require('path');

module.exports = {
  mode: 'development',
  entry: () => [
    'webpack-dev-server/client?http://localhost:3000',
    'webpack/hot/dev-server',
    './src/index.tsx'
  ],
  output: {
    filename: '[name].js',
    chunkFilename: '[name].chunk.js',
    path: path.resolve(__dirname, 'public'),
  },
  module: {
    rules: [{
      test: /\.(js|jsx|ts|tsx)$/,
      loader: 'babel-loader',
      exclude: /node_modules/
    },
    {
      test: /\.styl$/, 
      loader: 'style-loader!css-loader!stylus-loader'
    }]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx', '.styl']
  },
  stats: {
    colors: true
  },
  devtool: 'source-map',
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    hot: true,
    port: 3000,
    contentBase: './public',
    publicPath: '/',
    index: 'index.html',
    historyApiFallback: true,
  }
};