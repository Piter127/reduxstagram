import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Route, Switch } from 'react-router';

// redux
import { Provider } from 'react-redux';
import store from './store';

// Components
import Main from './components/Main';
import Single from './components/Single';
import PhotoGrid from './components/PhotoGrid';

// Global styles
import './styles/style.styl';

const router = (
  <Provider store={store}>
    <BrowserRouter>
      <Main>
        <Switch>
          <Route exact path="/" component={PhotoGrid} />
          <Route path="/view/:postId" component={Single} />
        </Switch>
      </Main>
    </BrowserRouter>  
  </Provider>
);

ReactDOM.render(
  router,
  document.getElementById('app')
);