import { createStore } from 'redux';
// import { syncHistoryWithStore } from 'react-router-redux';

// import the root reducer
import rootReducer from './reducers';

// data
import comments from './data/comments';
import posts from './data/posts';

// create an object for the defult data
const defaultState = {
  posts,
  comments
} as Object;

const store = createStore(rootReducer, defaultState);

export default store;
