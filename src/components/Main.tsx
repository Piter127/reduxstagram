import React, { ReactElement } from 'react';
import { Link } from 'react-router-dom';

const Main: React.StatelessComponent<any> = ({ children }) => (
  <div>
    <h1>
      <Link to="/">Reduxstagram</Link>
    </h1>
    {children}
  </div>
);

export default Main;
