import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';

interface SingleProps {
  posts: any[];
  comments: any[];
}

const mapStateToProps = (state) => ({
  posts: state.posts,
  comments: state.comments,
});

const mapDispatchToProps = (dispatch) => bindActionCreators(actionCreators, dispatch);

class Single extends React.Component<SingleProps> {
  render() {
    return (
      <div className="single-photo">
        single
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Single);