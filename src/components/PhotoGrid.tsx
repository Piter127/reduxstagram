import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actionCreators from '../actions/actionCreators';

// components
import Photo from './Photo';

interface PhotoGridProps {
  posts: any[];
  comments: any[];
}

const mapStateToProps = (state) => ({
  posts: state.posts,
  comments: state.comments,
});

const mapDispatchToProps = (dispatch) => bindActionCreators(actionCreators, dispatch);

class PhotoGrid extends React.Component<PhotoGridProps> {
  render() {
    const { posts } = this.props;
    return (
      <div className="photo-grid">
        {posts.map((post, i) => <Photo post={post} key={i} i={i} {...this.props} />)}
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PhotoGrid);