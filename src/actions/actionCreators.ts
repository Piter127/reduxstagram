// increment
export const increment = (index: string | number): Object => ({
  type: 'INCREMENT_LIKES',
  index,
});

// add comment
export const addComment = (postId: string, author: string, comment: string): Object => ({
  type: 'ADD_COMMENT',
  postId,
  author,
  comment
});

// remove comment
export const removeComment = (postId: string, i: any): Object => ({
  type: 'REMOVE_COMMENT',
  postId,
  i, 
});